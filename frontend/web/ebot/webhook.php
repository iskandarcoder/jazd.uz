<?php
define('token', '1204129883:AAEYDxZ174kJffcVX5syuQLcb4YCJUbVRKE');

include 'source.php';

//Check connection was successful
  if ($dblink->connect_errno) {
     printf("Failed to connect to database");
     exit();
  }
//baza bilan bog'lanish --------------------------------------------


switch ($text) {
	case '/start':
		$dblink->query("INSERT INTO `botuser` (`chat_id`, `first_name`, `last_name`, `username`) VALUES ('$chat_id', '$first_name', '$last_name', '$username')");

		$keyboard = array(array('Obmen', 'Prodaja'));
		$key = array(
			"resize_keyboard" => true,
			"keyboard" => $keyboard,
		);
		keyboard($key, $start_message, $chat_id);
		break;

        case 'Asosiy menyuga qaytish':
            $keyboard = array(array('Obmen', 'Prodaja'));
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard,
            );
            keyboard($key, $start_message, $chat_id);
            break;
        case 'Bekor qilish':
            $keyboard = [
                ['Asosiy menyuga qaytish']
            ];
            
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, 'Rahmat', $chat_id);
            break;
        case 'Tasdiqlash':
            $keyboard = [
                ['Asosiy menyuga qaytish']
            ];
            
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $end_message, $chat_id);
            break;
        case 'Obmen':
            
            $_SESSION[$chat_id]['obmen'] = 1;
            
            $keyboard = [
                ['Nexia', 'Spark', 'Lacetti'],
                ['Damas', 'Jiguli', 'Kamaz'],
                [' Isuzu 10t', 'Traktor', 'Matiz'],
                ['Malibu', 'Captiva', 'Isuzu Gazel'],
                ['Asosiy menyuga qaytish']
            ];
            
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_car, $chat_id);
            break;
        case 'Prodaja':
            
            $_SESSION[$chat_id]['obmen'] = 0;
            
            $keyboard = array(array('Главное меню'));
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard,
            );
            sendmessage($enter_car_quantity, $chat_id);
            break;
        
        case 'Nexia':
        case 'Spark':
        case 'Lacetti':
        case 'Damas':
        case 'Jiguli':
        case 'Kamaz':
        case 'Isuzu 10t':
        case 'Traktor':
        case 'Matiz':
        case 'Malibu':
        case 'Captiva':
        case 'Isuzu Gazel':
            
            $keyboard = array(array('Asosiy menyuga qaytish'));
                $key = array(
                    "resize_keyboard" => true,
                    "keyboard" => $keyboard,
                );
//                sendmessage($enter_car_quantity, $chat_id);
                keyboard($key, $enter_car_quantity, $chat_id);
                break;
        
        case 'Qoraqalpog‘iston':
            $_SESSION[$chat_id]['regionId'] = 8;
            $_SESSION[$chat_id]['regionName'] = 'Qoraqalpog‘iston';
        
            $_SESSION['regionId'] = 8;
        
            $keyboard = [
                ['Нукус шаҳар', 'Амударё', 'Беруний'],
                ['Қонликўл', 'Қораўзак', 'Кегейли'],
                ['Қўнғирот', 'Муйноқ', 'Нукус тумани'],
                ['Тахтакўприк', 'Тўрткўл', 'Хўжайли'],
                ['Чимбой', 'Шўманай', 'Элликқалъа'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Buxoro':
            $_SESSION[$chat_id]['regionId'] = 9;
            $_SESSION[$chat_id]['regionName'] = 'Buxoro';
            $keyboard = [
                ['Бухоро шаҳар', 'Бухоро туман', 'Вобкент'],
                ['Ғиждувон', 'Жондор', 'Когон туман'],
                ['Олот', 'Пешку', 'Ромитан'],
                ['Шофиркон', 'Қоракўл', 'Қоровулбозор'],
                ['Когон шаҳар'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Samarqand':
            $_SESSION[$chat_id]['regionId'] = 10;
            $_SESSION[$chat_id]['regionName'] = 'Samarqand';
            $keyboard = [
                ['Самарқанд шаҳар', 'Оқдарё', 'Булунғур'],
                ['Жомбой', 'Каттақўрғон тумани', 'Каттақўрғон шаҳар'],
                ['Қўшрабод', 'Нарпай', 'Нуробод'],
                ['Пайариқ', 'Пастдарғом', 'Пахтачи'],
                ['Самарқанд тумани', 'Тайлоқ', 'Ургут'],
                ['Иштихон'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Navoiy':
            $_SESSION[$chat_id]['regionId'] = 11;
            $_SESSION[$chat_id]['regionName'] = 'Navoiy';
            $keyboard = [
                ['Навоий', 'Кармана', 'Навбахор'],
                ['Нурота', 'Хатирчи', 'Қизилтепа'],
                ['Конимех', 'Учқудуқ', 'Зарафшон'],
                ['Томди'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Andijon':
            $_SESSION[$chat_id]['regionId'] = 12;
            $_SESSION[$chat_id]['regionName'] = 'Andijon';
            $keyboard = [
                ['Андижон шаҳар', 'Хонобод', 'Андижон тумани'],
                ['Асака', 'Балиқчи', 'Бўз'],
                ['Булоқбоши', 'Жалолқудуқ', 'Избоскан'],
                ['Улуғнор', 'Қўрғонтепа', 'Мархамат'],
                ['Олтинкўл', 'Пахтаобод', 'Ҳўжаобод'],
                ['Шахрихон'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Farg‘ona':
            $_SESSION[$chat_id]['regionId'] = 13;
            $_SESSION[$chat_id]['regionName'] = 'Farg‘ona';
            $keyboard = [
                ['Марғилон', 'Фарғона шаҳар', 'Қувасой'],
                ['Қўқон', 'Боғдод', 'Бешариқ'],
                ['Бувайда', 'Данғара', 'Ёзёвон'],
                ['Олтиариқ', 'Қўштепа', 'Риштон'],
                ['Сўх', 'Тошлоқ', 'Учкўприк', 'Қува'],
                ['Фарғона тумани', 'Фурқат', 'Ўзбекистон'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Surxondaryo':
            $_SESSION[$chat_id]['regionId'] = 14;
            $_SESSION[$chat_id]['regionName'] = 'Surxondaryo';
            $keyboard = [
                ['Ангор', 'Бойсун', 'Денов'],
                ['Жарқўрғон', 'Қизириқ', 'Қумқўрғон'],
                ['Музработ', 'Олтинсой', 'Сариосиё'],
                ['Термиз тумани', 'Термиз шаҳар', 'Узун'],
                ['Шеробод', 'Шўрчи'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Sirdaryo':
            $_SESSION[$chat_id]['regionId'] = 15;
            $_SESSION[$chat_id]['regionName'] = 'Sirdaryo';
            $keyboard = [
                ['Оқолтин', 'Боёвут', 'Гулистон'],
                ['Мирзаобод', 'Сайхунобод', 'Сирдарё'],
                ['Сардоба', 'Ховос', 'Гулистон'],
                ['Ширин', 'Янгиер'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Xorazm':
            $_SESSION[$chat_id]['regionId'] = 16;
            $_SESSION[$chat_id]['regionName'] = 'Xorazm';
            $keyboard = [
                ['Урганч шаҳар', 'Боғот', 'Гурлан'],
                ['Хозарасп', 'Хива', 'Хонқа'],
                ['Урганч тумани', 'Қўшкўприк', 'Шовот'],
                ['Янгиариқ', 'Янгибозор', 'Хива'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Toshkent viloyati':
            $_SESSION[$chat_id]['regionId'] = 17;
            $_SESSION[$chat_id]['regionName'] = 'Toshkent viloyati';
            $keyboard = [
                ['Ангрен', 'Бекобод шаҳар', 'Олмалиқ'],
                ['Чирчиқ', 'Бекобод тумани', 'Бўка'],
                ['Бўстонлиқ', 'Қибрай', 'Зангиота'],
                ['Қуйичирчиқ', 'Оққўрғон', 'Охонгарон'],
                ['Паркент', 'Пскент', 'Ўртачирчиқ'],
                ['Чиноз', 'Юқоричирчиқ', 'Янгийўл'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Qashqadaryo':
            $_SESSION[$chat_id]['regionId'] = 18;
            $_SESSION[$chat_id]['regionName'] = 'Qashqadaryo';
            $keyboard = [
                ['Қарши шаҳар', 'Ғузор', 'Қарши тумани'],
                ['Касби', 'Косон', 'Китоб'],
                ['Миришкор', 'Муборак', 'Нишон'],
                ['Чироқчи', 'Шахрисабз', 'Қамаши'],
                ['Дехқонобод', 'Яккабоғ'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Jizzax':
            $_SESSION[$chat_id]['regionId'] = 19;
            $_SESSION[$chat_id]['regionName'] = 'Jizzax';
            $keyboard = [
                ['Жиззах шаҳар', 'Бахмал', 'Дўстлик'],
                ['Ғаллаорол', 'Жиззах тумани', 'Зарбдор'],
                [' Зафаробод', 'Зомин', 'Пахтакор'],
                ['Мирзачўл', 'Фориш', 'Янгиобод'],
                ['Арнасой', 'Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Namangan':
            $_SESSION[$chat_id]['regionId'] = 21;
            $_SESSION[$chat_id]['regionName'] = 'Namangan';
            $keyboard = [
                ['Наманган шаҳар', 'Мингбулоқ', 'Поп'],
                ['Норин', 'Тўрақўрғон', 'Уйчи'],
                [' Чортоқ', 'Чуст', 'Янгиқўрғон'],
                ['Наманган тумани', 'Учқўрғон', 'Косонсой'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        case 'Toshkent shahar':
            $_SESSION[$chat_id]['regionId'] = 22;
            $_SESSION[$chat_id]['regionName'] = 'Toshkent shahar';
            $keyboard = [
                ['Юнусобод', 'Миробод', 'Яккасарой'],
                ['Олмазор', 'Бектемир', 'Яшнобод'],
                [' Чилонзор', 'Учтепа', 'Мирзо Улуғбек'],
                ['Сергели', 'Шайхонтохур'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_city, $chat_id);
            break;
        
        case '1':
            $_SESSION[$chat_id]['quantity'] = 1;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                ['Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '2':
            $_SESSION[$chat_id]['quantity'] = 2;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '3':
            $_SESSION[$chat_id]['quantity'] = 3;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '4':
            $_SESSION[$chat_id]['quantity'] = 4;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '5':
            $_SESSION[$chat_id]['quantity'] = 5;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                ['Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '6':
            $_SESSION[$chat_id]['quantity'] = 6;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '7':
            $_SESSION[$chat_id]['quantity'] = 7;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '8':
            $_SESSION[$chat_id]['quantity'] = 8;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '9':
            $_SESSION[$chat_id]['quantity'] = 9;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '10':
            $_SESSION[$chat_id]['quantity'] = 10;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '11':
            $_SESSION[$chat_id]['quantity'] = 11;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '12':
            $_SESSION[$chat_id]['quantity'] = 12;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '13':
            $_SESSION[$chat_id]['quantity'] = 13;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '14':
            $_SESSION[$chat_id]['quantity'] = 14;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '15':
            $_SESSION[$chat_id]['quantity'] = 15;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '16':
            $_SESSION[$chat_id]['quantity'] = 16;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '17':
            $_SESSION[$chat_id]['quantity'] = 17;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '18':
            $_SESSION[$chat_id]['quantity'] = 18;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '19':
            $_SESSION[$chat_id]['quantity'] = 19;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '20':
            $_SESSION[$chat_id]['quantity'] = 20;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '21':
            $_SESSION[$chat_id]['quantity'] = 21;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '22':
            $_SESSION[$chat_id]['quantity'] = 22;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '23':
            $_SESSION[$chat_id]['quantity'] = 23;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '24':
            $_SESSION[$chat_id]['quantity'] = 24;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '25':
            $_SESSION[$chat_id]['quantity'] = 25;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '26':
            $_SESSION[$chat_id]['quantity'] = 26;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '27':
            $_SESSION[$chat_id]['quantity'] = 27;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '28':
            $_SESSION[$chat_id]['quantity'] = 28;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '29':
            $_SESSION[$chat_id]['quantity'] = 29;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        case '30':
            $_SESSION[$chat_id]['quantity'] = 30;
            $keyboard = [
                ['Qoraqalpog‘iston', 'Buxoro', 'Samarqand'],
                ['Navoiy', 'Andijon', 'Farg‘ona'],
                [' Surxondaryo', 'Sirdaryo', 'Xorazm'],
                ['Toshkent viloyati', 'Qashqadaryo', 'Jizzax'],
                ['Namangan', 'Toshkent shahar'],
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $choose_region, $chat_id);
            break;
        
        
        case 'Нукус шаҳар':
            $_SESSION[$chat_id]['cityId'] = 10;
            $keyboard = [
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $enter_phone_number, $chat_id);
            break;
        case 'Амударё':
        case 'Беруний':
        case 'Қонликўл':
        case 'Қораўзак':
        case 'Кегейли':
        case 'Қўнғирот':
        case 'Муйноқ':
        case 'Нукус тумани':
        case 'Тахтакўприк':
        case 'Тўрткўл':
        case 'Хўжайли':
        case 'Чимбой':
        case 'Шўманай':
        case 'Элликқалъа':
        case 'Бухоро шаҳар':
        case 'Бухоро туман':
        case 'Вобкент':
        case 'Ғиждувон':
        case 'Жондор':
        case 'Когон':
        case 'Олот':
        case 'Пешку':
        case 'Ромитан':
        case 'Шофиркон':
        case 'Қоракўл':
        case 'Қоровулбозор':
        case 'Самарқанд шаҳар':
        case 'Оқдарё':
        case 'Булунғур':
        case 'Жомбой':
        case 'Каттақўрғон тумани':
        case 'Каттақўрғон шаҳар':
        case 'Қўшрабод':
        case 'Нарпай':
        case 'Нуробод':
        case 'Пайариқ':
        case 'Пастдарғом':
        case 'Пахтачи':
        case 'Самарқанд тумани':
        case 'Тайлоқ':
        case 'Ургут':
        case 'Навоий':
        case 'Кармана':
        case 'Навбахор':
        case 'Нурота':
        case 'Хатирчи':
        case 'Қизилтепа':
        case 'Конимех':
        case 'Учқудуқ':
        case 'Зарафшон':
        case 'Томди':
        case 'Андижон шаҳар':
        case 'Хонобод':
        case 'Андижон тумани':
        case 'Асака':
        case 'Балиқчи':
        case 'Бўз':
        case 'Булоқбоши':
        case 'Жалолқудуқ':
        case 'Избоскан':
        case 'Улуғнор':
        case 'Қўрғонтепа':
        case 'Мархамат':
        case 'Олтинкўл':
        case 'Пахтаобод':
        case 'Ҳўжаобод':
        case 'Шахрихон':
        case 'Марғилон':
        case 'Фарғона шаҳар':
        case 'Қувасой':
        case 'Қўқон':
        case 'Боғдод':
        case 'Бешариқ':
        case 'Бувайда':
        case 'Данғара':
        case 'Ёзёвон':
        case 'Олтиариқ':
        case 'Қўштепа':
        case 'Риштон':
        case 'Сўх':
        case 'Тошлоқ':
        case 'Учкўприк':
        case 'Фарғона тумани':
        case 'Фурқат':
        case 'Ўзбекистон':
        case 'Қува':
        case 'Ангор':
        case 'Бойсун':
        case 'Денов':
        case 'Жарқўрғон':
        case 'Қизириқ':
        case 'Қумқўрғон':
        case 'Музработ':
        case 'Олтинсой':
        case 'Сариосиё':
        case 'Термиз тумани':
        case 'Термиз шаҳар':
        case 'Узун':
        case 'Шеробод':
            $_SESSION[$chat_id]['cityId'] = 113;
            $keyboard = [
                ['Asosiy menyuga qaytish']
            ];
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $enter_phone_number, $chat_id);
            break;

        case (preg_match('/^\d{9}$/', $text) ? true : false) :
            
            $keyboard = [
                ['Asosiy menyuga qaytish']
            ];
            
            $key = array(
                "resize_keyboard" => true,
                "keyboard" => $keyboard
            );
            keyboard($key, $end_message, $chat_id);
            break;

//            $keyboard = [
//                ['Tasdiqlash', 'Bekor qilish']
//            ];
//            $key = array(
//                "resize_keyboard" => true,
//                "keyboard" => $keyboard
//            );
//
//        $natija = "\n".'Soni: '.$_SESSION[$chat_id]['quantity'];
//        $natija .= "\n".'Viloyat: '.$_SESSION['regionId'];
//
//            keyboard($key, $please_confirm.$natija, $chat_id);
//            break;

	default:
		send($chat_id, "Siz bizda mavjud bo'lmagan buyrug'ni berdingiz");
		break;
}

//START callback uchun
if(callback($update)){
	switch ($call_data) {
		case '0':
			send($call_id, "Siz Maishiy tehnikani kategoriyasini tanladingiz");
			break;
		
		default:
			send($call_id, "Bot ustida dasturlash ishlari olib borilmoqda");
			break;
	}
}
?>
