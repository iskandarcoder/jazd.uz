<?php

namespace frontend\controllers;

use Yii;
use backend\models\Zayavka;
use backend\models\Service;
use backend\models\Cost;
use backend\models\Town;
use backend\models\District;
use backend\models\ZayavkaType;
use backend\models\ZayavkaSearch;
//use yii\web\Controller;
use frontend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ZayavkaController implements the CRUD actions for Zayavka model.
 */
class ZayavkaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Zayavka models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZayavkaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zayavka model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $services = Service::find()->andWhere(['active'=>[1]])->orderBy('order_s ASC')->all();
      $cost = Cost::find()->orderBy('date DESC')->one();
      $zayavka = Zayavka::find()->andWhere(['id' => $id])->with('energy')->all();
      $zayavkatype = ZayavkaType::find()->andWhere(['zayavka_id' => $id])->with('tenergy')->all();
      //echo "<pre>"; print_r($zayavka); die;
        return $this->render('view', [
            'model' => $this->findModel($id),
            'services' => $services,
            'cost' => $cost,
            'zayavka' => $zayavka,
            'zayavkatype' => $zayavkatype,
        ]);
    }

    public function actionTown($id)
    {   
      $town_count = Town::find()->andWhere(['region_id'=>$id])->count();  
      $town = Town::find()->andWhere(['region_id'=>$id])->all();

      if($town_count > 0){
          foreach ($town as $key => $value) {
              echo "<option value='".$value->id."'>".$value->name."</option>";
          }
      }else{
        echo "<option></option>";
      }
      
    }

    /**
     * Creates a new Zayavka model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($energy_id=NULL)
    {

        $model = new Zayavka();
        $services = Service::find()->andWhere(['active'=>[1]])->orderBy('order_s ASC')->all();
        // echo $energy_id;die;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          $model->status = 1;
          $model->save();
          $count=(array)Yii::$app->request->post('count');
          //echo "<pre>";print_r($count);die;
          foreach ($count as $key => $value) {

            if($value !=''){
              Yii::$app->db->createCommand()->insert('zayavka_type', [
                  'zayavka_id' => $model->id,
                  'type_energy_id' => $key,
                  'count' => $value,
              ])->execute();
            }
          }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
          'services' => $services,
          'model' => $model,
          'energy_id' => $energy_id,
        ]);
    }

    /**
     * Updates an existing Zayavka model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Zayavka model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    /**
     * Finds the Zayavka model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Zayavka the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Zayavka::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionDistrictList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = [];
        if (isset($_GET['id'])) {
            // var_dump($_POST);exit();
            $parents = $_GET['id'];
            if ($parents != null) {
                $region_id = $parents;
                if ($region_id) {
                    $out = Town::all($region_id, false);
                } else {
                    $out = '';
                }
                return ['output' => $out];
                return ['output' => $out, 'selected' => ''];
            }
        }

        return ['output' => '', 'selected' => ''];
    }
}
