<?php

namespace frontend\controllers;

use Yii;
use backend\models\Page;
use backend\models\PageSearch;
use frontend\components\BaseController;
use yii\web\NotFoundHttpException;
use backend\models\Service;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new PageSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        $model = Page::find()->andWhere(['id'=>[$id]])->One();
        $services = Service::find()->andWhere(['active'=>[1]])->orderBy('order_s ASC')->all();

        return $this->render('view', [
            'services' => $services,
            'model' => $model,
        ]);
    }


    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
