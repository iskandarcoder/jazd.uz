<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Newpage */

$this->title = 'Create Newpage';
$this->params['breadcrumbs'][] = ['label' => 'Newpages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newpage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
