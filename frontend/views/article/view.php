<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Menu;
use backend\models\SubMenu;
use backend\models\Symbol;
use backend\models\TopMenu;
use yii\helpers\Url;

$menus = Menu::find()->andWhere(['active'=>[1]])->all();
$symbol= Symbol::find()->all();
$topmenus = TopMenu::find()->all();
$lang = Yii::$app->language;

/* @var $this yii\web\View */
/* @var $model backend\models\Article */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

<div class="col-lg-10 col-lg-offset-1" id="wrapper"><br>

    <div class="container">

        <div class="row">
                        <div class="col-md-9">

                            <div class="row">
                                <div class="col-md-12">
                                    <h4 style="text-align: justify;">
                                        <a href="<?= Url::to(['/site/about'])?>"><i class="fa fa-arrow-left"></i> <?= Yii::t('app', 'konstitutsiya') ?></a>
                                    </h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><br>
                                    <?= $model[$lang.'_article_tex'];?>
                                   
                                </div>
                            </div>

                        </div>

                        <div class="col-md-3">
                            <div class="toggle toggle-primary" data-plugin-toggle>
                                <?php $i=0;?>
                                <?php foreach ($menus as $key => $menu): ?>
                                    <?php $i++;?>
                                    <section class="toggle <?php if($i==1):?>active<?php endif;?>">
                                        <label><?=$menu->{$lang.'_name'}?></label>
                                        <?php $sub_menus = SubMenu::find()->where(['parent' => $menu->id])->all(); ?>
                                        <div class="toggle-content">
                                            <ul class="dropdown-mega-sub-nav">
                                                <?php foreach ($sub_menus as $key => $sub_menu): ?>
                                                    <li><a <?php if($sub_menu->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $sub_menu->page  ])?>"<?php endif;?> href="<?= $sub_menu->link;?>" <?php if($sub_menu->target==1):?> target="_blank"<?php endif;?> ><?= $sub_menu->{$lang.'_name'}; ?></a></li>
                                                 <?php endforeach?>    
                                            </ul>
                                        </div>
                                    </section>
                                <?php endforeach;?>
                                
                            </div>
                            <?php foreach($symbol as $key => $sym):?>
                            <div class="row">
                                <center>
                               <h4><?= $sym[$lang.'_name']?></h4><hr>
                                </center>   
                               <a  <?php if($sym->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $sym->page  ])?>"<?php endif;?> href="<?= $sym->link;?>" <?php if($sym->link!=NULL):?> target="_blank"<?php endif;?> ><img  src="<?= Yii::getAlias('@web') ?>/uploads/<?= $sym->img;?>"></a>
                            </div>
                          <?php endforeach;?>
                        </div>
                    </div><br><br>

    

</div>
</div>
</div>
