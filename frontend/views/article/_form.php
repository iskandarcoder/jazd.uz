<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'chapter_id')->textInput() ?>

    <?= $form->field($model, 'uz_article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ru_article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'x_article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uz_article_tex')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ru_article_tex')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'en_article_tex')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'x_article_tex')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
