<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Menu;
use backend\models\SubMenu;
use backend\models\Symbol;
use backend\models\TopMenu;
use yii\helpers\Url;

$menus = Menu::find()->andWhere(['active'=>[1]])->all();
//$symbol= Symbol::find()->all();
//$topmenus = TopMenu::find()->all();
$lang = Yii::$app->language;
//    $this->title = $model->{$lang.'_name'};

/* @var $this yii\web\View */
/* @var $model backend\models\Page */

//$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-content shop-page main-content-detail">
  <div class="container">
    <br>
    <div class="row">
      <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 content-offset">
        <div class="about-product row">
          <h3><?= $model{$lang.'_name'};?></h3>
          <hr>
          <p><?= $model{$lang.'_text'};?></p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 sidebar">
        <div class="equal-container widget-featrue-box">
          <div class="row">
            <?php foreach ($services as $key => $service):?>
            <div class="col-ts-12 col-xs-4 col-sm-12 col-md-12 col-lg-12 featrue-item">
              <div class="featrue-box layout2 equal-elem">
                <div class="block-icon">
                  <a <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?>>
                    <?php if($service->icon_style):?><span class="<?= $service->icon_style;?>"><?php endif;?></span>
                  </a>
                </div>
                <div class="block-inner">
                  <a <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?> class="title">
                    <?= $service->{$lang.'_name'};?>
                  </a>
                  <p class="des"><?= $service->{$lang.'_description'};?></p>
                </div>
              </div>
            </div>
          <?php endforeach;?>
          </div>
        </div>
        <div class="widget widget-banner row-banner">
          <div class="banner banner-effect1">
            <a href="#"><img src="images/banner23.jpg" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
