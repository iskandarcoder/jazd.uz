<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\Online */
/* @var $form yii\widgets\ActiveForm */
$reference_subject = [
    'Konsullik masalalari' => Yii::t('app', 'Konsullik masalalari'),
    'Boshqalar' => Yii::t('app', 'Boshqalar'),
];

?>
<div class="col-lg-10 col-lg-offset-1" id="wrapper"><br>

    <div class="container">

        <div class="row">
            <h4><?= Yii::t('app', 'Contact Us') ?></h4>
            <div class="col-md-5">
                <div class="online-form">

                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'reference_subject')->dropDownList($reference_subject,['prompt'=>Yii::t('app', 'variant')]) ?>

                    <?= $form->field($model, 'telefon')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'reference_text')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

                    <div class="form-group" style="float: right;">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'send') : Yii::t('app', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
            <div class="col-md-7" >
                <p><iframe frameborder="0" height="300" src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d676.326221029278!2d-77.04088065477485!3d38.90840509615396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x89b7b7c71560de47%3A0xf8b4e06cff091a65!2sEmbassy+of+Uzbekistan%2C+1746+Massachusetts+Ave+NW%2C+Washington%2C+DC+20036!3m2!1d38.9083604!2d-77.04051009999999!5e1!3m2!1suz!2s!4v1517311083479" style="border:0" width="700"></iframe></p>
                
                <h4 class="heading-primary">O‘zbekiston Respublikasining Amerika Qo‘shma Shtatlaridagi Favqulodda va Muxtor Elchisining fuqarolarni qabul qilish jadvali</h4>
                <ul class="list list-icons list-dark mt-xlg">
                    <li><i class="fa fa-clock-o"></i>&nbsp; &nbsp; &nbsp; Monday - Friday 9am to 5pm</li>
                    <li><i class="fa fa-clock-o"></i>&nbsp; &nbsp; &nbsp; Saturday - 9am to 2pm</li>
                    <li><i class="fa fa-clock-o"></i>&nbsp; &nbsp; &nbsp; Sunday - Closed</li>
                </ul>

            </div>
        </div>
    </div>
</div>
