<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Energy;
use backend\models\TypeEnergy;
use backend\models\Region;
use backend\models\Town;
use yii\helpers\Url;
use kartik\select2\Select2;
use backend\models\CategoryZayavka;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model backend\models\Zayavka */
/* @var $form yii\widgets\ActiveForm */
$lang = Yii::$app->language;
$energy = Energy::find()->andWhere(['id'=>$energy_id])->orderBy('id DESC')->one();
$typenergy = TypeEnergy::find()->all();
$town = array();
?>
<style>
.kolonka {
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-auto-rows: 2em;
  grid-gap: .5em;
}
.kolonka input{
  width: 50px;
}
</style>
<div class="energy-view">



    <div class="main-content shop-page main-content-detail">
  		<div class="container">
  			<div class="breadcrumbs">
  				<a href="<?= Url::to(['/']) ?>">jazd.uz</a> \ <span class="current"><?= Yii::t('app', 'detail') ?></span>
  			</div>
  			<div class="row">
  				<div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 content-offset">
  					<div class="about-product row">
  						<div class="details-thumb col-xs-12 col-sm-12 col-md-6 col-lg-6">
  							<div class="owl-carousel nav-style3 has-thumbs" data-autoplay="false" data-nav="true" data-dots="false" data-loop="true" data-slidespeed="800" data-margin="0"  data-responsive = '{"0":{"items":1}, "480":{"items":2}, "768":{"items":1}, "1024":{"items":1}, "1200":{"items":1}}'>
  								<div class="details-item"><img src="/uploads/<?= $energy->img1;?>" alt=""></div>
  								<div class="details-item"><img src="/uploads/<?= $energy->img2;?>" alt=""></div>
  								<div class="details-item"><img src="/uploads/<?= $energy->img3;?>" alt=""></div>
  								<div class="details-item"><img src="/uploads/<?= $energy->img4;?>" alt=""></div>
  							</div><br><br>
                <a class="product-name"><?= $energy['name_'.$lang];?></a>
  							<hr>
  							<div class="price">
                  <?php if($energy->sell):?><s><h6><?= $energy->cost;?> <?= Yii::t('app', 'so\'m') ?></h6></s><?php else:?><?= $energy->cost;?> <?= Yii::t('app', 'so\'m') ?><?php endif;?> </span>
                  <?php if($energy->sell):?><span class="ins"><?= $energy->sell;?> <?= Yii::t('app', 'so\'m') ?></span><?php endif;?>
  							</div>
                <hr>
  						</div>
  						<div class="details-info col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'category_zayavka')->dropDownList(
                    ArrayHelper::map(CategoryZayavka::find()->all(), 'id','name'),
                    ['prompt'=>'Выберите ...']
                    ) ?>
                <div class="kolonka">
                  <?php foreach ($typenergy as $key => $type):?>
                    <table>
                      <tr>
                        <td style="width:30px;"><label><?= $type->amper;?> </td>
                          <td>
                            <input type="text" name="count[<?= $type->id;?>]" class="input-info" width="20"> Dona</label>
                          </td>
                      </tr>
                    </table>

                  <?php endforeach;?>
                </div>
                  <?= $form->field($model, 'energy_id')->hiddenInput(['value'=> $energy_id])->label(false);?>
                      <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
                      <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
                      <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                      <?= $form->field($model, 'region_id')->dropDownlist(ArrayHelper::map(Region::find()->all(), 'id','name'),   [
                                      'prompt' => '---',
                                      'onchange' => 'loadDistricts()'
                                      ]);
                                       ?>

                         <?php
                         //   if(\Yii::$app->request->isAjax){
                         //  var_dump(Yii::$app->request->get('id'));exit();
                         // $id = Yii::$app->request->get('id');
                         // }
                         // else{
                         //  $id = 22;
                         // }
                       
                      // if ($_GET) {
                      //   $id = $_GET['id'];
                      //  } 
                       ?>
                      <?= $form->field($model, 'district_id')->dropDownlist(
                        ArrayHelper::map(Town::find()->all(), 'region_id','name'),[
                        'prompt' => '---',
                        ]
                      ); ?>
                  <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>


                <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 sidebar">
  					<div class="equal-container widget-featrue-box">
  						<div class="row">
                <?php foreach ($services as $key => $service):?>
  							<div class="col-ts-12 col-xs-4 col-sm-12 col-md-12 col-lg-12 featrue-item">
  								<div class="featrue-box layout2 equal-elem">
  									<div class="block-icon">
                      
                        <?php if($service->icon_style):?><span class="<?= $service->icon_style;?>"><?php endif;?></span>
                      
                    </div>
  									<div class="block-inner">
                        <?= $service->{$lang.'_name'};?>
                    
  										<p class="des"><?= $service->{$lang.'_description'};?></p>
  									</div>
  								</div>
  							</div>
              <?php endforeach;?>
  						</div>
  					</div>
  					<div class="widget widget-banner row-banner">
  						<div class="banner banner-effect1">
  							<a href="#"><img src="images/banner23.jpg" alt=""></a>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>

<script type="text/javascript">
  function loadDistricts(){
var id = $('#zayavka-region_id').val();
        $.ajax({
            type: 'get',
            url: '<?= yii\helpers\Url::to(["zayavka/district-list"]) ?>',
            data: {id: id},
            dataType: 'json',
            success: function (res) {
              console.log(res.output)

              var options = res.output;

              var optionsHtml = '';
              for (var i = options.length - 1; i >= 0; i--) {
                optionsHtml += '<option value="'+options[i].id+'">'+options[i].name+'</option>';
              }
                $('#zayavka-district_id').html(optionsHtml);
                $data = res;
                // console.log(res);
                // $('.create-update-school').modal('show');
            },
            error: function (e) {
                console.log(e);
            }
        });
  }  
    <?php ob_start() ?>
  
// $('body').on( 'change','#zayavka-region_id', function () {
        
//     });

    
    // $(document).ready(function () {

    //     $("#zayavka-region_id").onchange(function (event) {
    //         // console.log(alert("You have Selected  :: "+$(this).val()));

    //     });
    // });
    /* function onChange() {
           var data = document.querySelector("#zayavka-region_id");
           console.log(data);
         } */


    <?php $this->registerJs(ob_get_clean()) ?>
</script>



  	</div>
