<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ZayavkaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Zayavkas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zayavka-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Zayavka', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'energy_id',
            'fio',
            'telephone',
            'email:email',
            //'region_id',
            //'district_id',
            //'address',
            //'old_energy',
            //'count_energy',
            //'status',
            //'text:ntext',
            //'date',
            //'user_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
