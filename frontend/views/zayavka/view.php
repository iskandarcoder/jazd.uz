<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Zayavka */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Zayavkas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$lang = Yii::$app->language;
?>
<div class="main-content shop-page main-content-detail">
  <div class="container">
    <br>
    <div class="row">
      <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 ">
        <div class="about-product row">
          <h4><?= Yii::t('app', 'Sizning buyurtmangiz qabul qilindi. Bizning operatorlar tez orada aloqaga chiqadi.') ?></h4>
          <hr>
          <?php $sum = 0;?>
          <?php foreach ($zayavkatype as $key => $ztype) {
                      $sum += $ztype->tenergy->weight * $cost->name;
                }
                //echo $sum;
          ?>
          <?php foreach ($zayavka as $key => $item) {
                  if($item->energy->sell==''){
                    $cost_energy = $item->energy->cost;
                  }else{
                    $cost_energy = $item->energy->sell;
                  }
                  $energy_name = $item->energy['name_'.$lang];
                  }
                //echo $cost_energy;
          ?>
          <?= $energy_name;?> - <?= $cost_energy;?> <?= Yii::t('app', 'so\'m') ?><hr><br>

          <?= $cost_energy;?> <?= Yii::t('app', 'so\'m') ?> - <?= $sum;?> <?= Yii::t('app', 'so\'m') ?> = <?= $cost_energy - $sum?> <?= Yii::t('app', 'so\'m') ?>
          <hr>
            <h3><?= Yii::t('app', 'Tahminiy narx'); ?> - <?= $cost_energy - $sum?> <?= Yii::t('app', 'so\'m') ?></h3>
        </div>
      </div>
      <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 sidebar">
        <div class="equal-container widget-featrue-box">
          <div class="row">
            <?php foreach ($services as $key => $service):?>
            <div class="col-ts-12 col-xs-4 col-sm-12 col-md-12 col-lg-12 featrue-item">
              <div class="featrue-box layout2 equal-elem">
                <div class="block-icon">
                  <a <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?>>
                    <?php if($service->icon_style):?><span class="<?= $service->icon_style;?>"><?php endif;?></span>
                  </a>
                </div>
                <div class="block-inner">
                  <a <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?> class="title">
                    <?= $service->{$lang.'_name'};?>
                  </a>
                  <p class="des"><?= $service->{$lang.'_description'};?></p>
                </div>
              </div>
            </div>
          <?php endforeach;?>
          </div>
        </div>
        <div class="widget widget-banner row-banner">
          <div class="banner banner-effect1">
            <a href="#"><img src="images/banner23.jpg" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
