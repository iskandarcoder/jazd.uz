<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Zayavka */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Zayavkas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zayavka-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'services' => $services,
        'model' => $model,
        'energy_id' => $energy_id,
    ]) ?>

</div>
