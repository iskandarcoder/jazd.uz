<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use backend\models\Menu;
use backend\models\SubMenu;
use backend\models\Symbol;
use backend\models\TopMenu;
use backend\models\Chapter;
use backend\models\Page ;
use backend\models\Article;


use yii\helpers\Url;

$menus = Menu::find()->andWhere(['active'=>[1]])->all();
$symbol= Symbol::find()->all();
$topmenus = TopMenu::find()->all();
$guzb = Page::find()->andWhere(['id'=>[19]])->all();
$lang = Yii::$app->language;

$this->title = 'Map';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="page-view">
	<div class="col-lg-10 col-lg-offset-1" id="wrapper"><br>

    <div class="container">
    	<div class="row">
    		<div class="col-md-9">
    			<?php foreach ($guzb as $key => $uzb):?>
    				<?php echo $uzb[$lang.'_text'];?>
    			<?php endforeach;?>
    	
	<script src="<?= Url::to(['/js/highmaps.js']) ?>"></script>
	<script src="<?= Url::to(['/js/'.$lang.'-all.js']) ?>"></script>
	<style type="text/css">
	  #container {
      height: 500px; 
      min-width: 310px; 
      max-width: 800px; 
     /* margin: 0 auto; */
  }
  .loading {
      margin-top: 10em;
      text-align: center;
      color: gray;
  }

</style>
<div id="container"></div>

</div>
<div class="col-md-3">
	<div class="toggle toggle-primary" data-plugin-toggle>
                                <?php $i=0;?>
                                <?php foreach ($menus as $key => $menu): ?>
                                    <?php $i++;?>
                                    <section class="toggle <?php if($i==1):?>active<?php endif;?>">
                                        <label><?=$menu->{$lang.'_name'}?></label>
                                        <?php $sub_menus = SubMenu::find()->where(['parent' => $menu->id])->all(); ?>
                                        <div class="toggle-content">
                                            <ul class="dropdown-mega-sub-nav">
                                                <?php foreach ($sub_menus as $key => $sub_menu): ?>
                                                    <li><a <?php if($sub_menu->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $sub_menu->page  ])?>"<?php endif;?> href="<?= $sub_menu->link;?>" <?php if($sub_menu->target==1):?> target="_blank"<?php endif;?> ><?= $sub_menu->{$lang.'_name'}; ?></a></li>
                                                 <?php endforeach?>    
                                            </ul>
                                        </div>
                                    </section>
                                <?php endforeach;?>
                            </div>
                             <?php foreach($symbol as $key => $sym):?>
                            <div class="row">
                                <center>
                               <h4><?= $sym[$lang.'_name']?></h4><hr>
                                </center>   
                               <a  <?php if($sym->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $sym->page  ])?>"<?php endif;?> href="<?= $sym->link;?>" <?php if($sym->link!=NULL):?> target="_blank"<?php endif;?> ><img  src="<?= Yii::getAlias('@web') ?>/uploads/<?= $sym->img;?>"></a>
                            </div>
                          <?php endforeach;?>
</div>
<script type="text/javascript">
	// Prepare demo data
// Data is joined to map using value of 'hc-key' property by default.
// See API docs for 'joinBy' for more info on linking data and map.
var data = [
    ['uz-fa', 0],
    ['uz-tk', 20],
    ['uz-an', 2],
    ['uz-ng', 3],
    ['uz-ji', 4],
    ['uz-si', 5],
    ['uz-ta', 6],
    ['uz-bu', 7],
    ['uz-kh', 8],
    ['uz-qr', 9],
    ['uz-nw', 10],
    ['uz-sa', 11],
    ['uz-qa', 12],
    ['uz-su', 13]
];

// Create the chart
Highcharts.mapChart('container', {
    chart: {
        map: 'countries/uz/uz-all'
    },

    title: {
       // text: 'Highmaps basic demo'
    },

    subtitle: {
    //  text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/uz/uz-all.js">Uzbekistan</a>'
    },

    mapNavigation: {
        enabled: false,
        buttonOptions: {
            verticalAlign: 'bottom'
        }
    },

    colorAxis: {
        min: 0
    },

    series: [{
        data: data,
        //name: 'Random data',
        states: {
            hover: {
                color: '#BADA55'
            }
        },
        dataLabels: {
           // enabled: true,
           // format: '{point.name}'
        }
    }]
});

</script>
</div>
</div>
</div></div>