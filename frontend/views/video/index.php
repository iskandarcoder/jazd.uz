<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Menu;
use backend\models\SubMenu;
use backend\models\Symbol;
use backend\models\TopMenu;
use yii\widgets\LinkPager;
use yii\helpers\Url;
$lang = Yii::$app->language;
    //$this->title = $model->{$lang.'_name'};
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Videos';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page -->
  <div class="page animsition" style="top: 40px;">
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-md-9">
          <!-- Panel -->
          <div class="panel">
            <div class="panel-body nav-tabs-animate nav-tabs-horizontal">
              <div class="tab-content">
                <div class="tab-pane active animation-slide-left" id="activities" role="tabpanel">
                  <h4>Video</h4>
                  <hr>
                    <?php foreach ($models as $key => $video):?>
                        <div class="col-sm-4">
                            <div class="example">
                              <iframe class="cover-iframe  height-xs-285" src="<?= $video->frame;?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;loop=1&amp;modestbranding=1&amp;wmode=transparent&amp;enablejsapi=1&amp;api=1"></iframe>
                            </div>
                        </div>

                     <?php endforeach;?>  <br>
                     <?php
                                        echo LinkPager::widget([
                                            'pagination' => $pages,
                                        ]);
                                    ?> 
                </div>
              </div>
            </div>
          </div>
          <!-- End Panel -->
        </div>
        <div class="col-md-3">
          <!-- Page Widget -->
          <div class="widget widget-shadow text-center">
            <div class="widget-header">
              <div class="widget-header-content">
                    <?php foreach ($services as $key => $service):?>
                        <div class="col-lg-12 col-md-12" >
                          <div class="widget widget-shadow text-center">
                            <div class="widget-header cover overlay" style="height: calc(100% - 100px);">
                              <img class="cover-image" src="/images/ppp.jpg" alt="..." style="height: 100%;">
                              <div class="overlay-panel vertical-align">
                                <div class="vertical-align-middle">
                                  <a class="avatar avatar-100 bg-white margin-bottom-10 margin-xs-0 img-bordered" <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?> style="width: 214px;border-radius: 0px;">
                                    <img src="/uploads/<?= $service->icon;?>" alt="" style="border-radius: 0px;">
                                  </a>
                                  <div class="font-size-15" style="color: #0074b4"><?= $service->{$lang.'_name'};?></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <?php endforeach;?>
                
              </div>
            </div>
          </div>
          <!-- End Page Widget -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->