<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\models\Menu;
use backend\models\Sayt;
use backend\models\Car;
use backend\models\SubMenu;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use backend\models\News;
use yii\widgets\ActiveForm;


$menus = Menu::find()->andWhere(['active'=>'1'])->orderBy('order ASC')->all();
$sayts = Sayt::find()->orderBy('id DESC')->all();
$cars = Car::find()->orderBy('id DESC')->all();
$lang = Yii::$app->language;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
    <title><?= Yii::t('app', 'Embassy_in') ?></title>
    <?php $this->head() ?>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

</head>
<?php $this->beginBody() ?>
<body class="home home<?= param('themeVariant') ?>">
	<header>
		<div class="header layout1 header-home2 no-prepend-box-sticky">
			<div class="topbar layout1">
				<div class="container">
					<ul class="menu-topbar">
						<li class="language menu-item-has-children">
							<a href="#" class="toggle-sub-menu"><span class="flag"><img src="/images/<?= $lang?>.png" alt=""></span><?php if($lang =='uz'){echo "O'zbekcha";}else{echo "Русский";}?></a>
							<ul class="list-language sub-menu">
								<li>
                  <?= Html::a('<span ></span> O\'zbekcha', array_merge(
                                                            \Yii::$app->request->get(),
                                                            [\Yii::$app->controller->route, 'language' => 'uz']
                                                          ),
                                                          [
                                                            'class' => 'language'
                                                          ]
                                                        ); ?>
                </li>
								<li>
                  <?= Html::a('<span ></span> Русский', array_merge(
                                                            \Yii::$app->request->get(),
                                                            [\Yii::$app->controller->route, 'language' => 'ru']
                                                          ),
                                                          [
                                                            'class' => 'language'
                                                          ]
                                                        ); ?>
                </li>
							</ul>
						</li>

					</ul>
					<?php foreach ($sayts as $key => $sayt):?>
					<ul class="list-socials">
						<li><a href="<?= $sayt->facebook;?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<!-- <li><a href="<?= $sayt->twitter;?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li> -->
						<li><a href="<?= $sayt->instagram;?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				<?php endforeach;?>
				</div>
			</div>
			<div class="main-header">
				<div class="top-header">
					<div class="container">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12  left-content">
								<div class="logo">
									<a href="<?= Url::to(['/']) ?>"><img src="/images/logo.png" alt=""></a>
								</div>
							</div>
							<div class="col-lg-6 col-md-5 col-sm-6 col-xs-12 midle-content">
								<div class="search-form layout1 box-has-content">
									<div class="search-block">
										<div class="search-inner">
											<input type="text" class="search-info" placeholder="<?= Yii::t('app', 'Search') ?>...">
										</div>

										<a href="#" class="search-button"><i class="fa fa-search" aria-hidden="true"></i></a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 right-content">
								<ul class="header-control">
									<li class="hotline">
										<div class="icon">
											<i class="fa fa-phone" aria-hidden="true"></i>
										</div>
										<div class="content">
											<span class="text"><?= Yii::t('app', 'Murojaat uchun') ?></span>
											<span class="number"><a href="tel:<?= $sayt->fax;?>">tel:<?= $sayt->fax;?></a></span>
										</div>
									</li>
									<li class="box-minicart">

									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="this-sticky">
					<div class="header-nav-wapper ">
						<div class="container main-menu-wapper">
							<div class="row">
								<div class="col-lg-3 col-md-4 col-sm-12">
									<div class="vertical-wapper parent-content">
										<div class="block-title show-content">
											<span class="icon-bar">
												<span></span>
												<span></span>
												<span></span>
											</span>
											<span class="text"><?= Yii::t('app', 'Avtomobil turi bo\'yicha') ?></span>
										</div>
										<a href="#" class="header-top-menu-mobile"><span class="fa fa-cog" aria-hidden="true"></span></a>
										<a class="menu-bar mobile-navigation " href="#">
						                        <span class="icon">
						                            <span></span>
						                            <span></span>
						                            <span></span>
						                        </span>
						                        <span class="text"> Menu</span>
						                    </a>

										<div class="vertical-content hidden-content">
											<ul class="vertical-menu ovic-clone-mobile-menu">
												<?php foreach($cars as $key => $car):?>
													<li><a href="<?= Url::to(['/site/filter', 'car_id' => $car->id])?>" class="ovic-menu-item-title" title="Cameras"><?= $car['name_'.$lang]?></a></li>
												<?php endforeach;?>
												</ul>
										</div>
									</div>
								</div>
<div class="col-lg-9 col-md-8 col-sm-12">
	<div class="header-nav container-vertical-wapper">
		<div class="header-nav-inner">
			<div class="box-header-nav">
				<div class=" container-wapper">
					<a class="menu-bar mobile-navigation" href="#">
						<span class="icon">
							<span></span>
							<span></span>
							<span></span>
						</span>
						<span class="text">Main Menu</span>
					</a>
					<a href="#" class="header-top-menu-mobile"><span class="fa fa-cog" aria-hidden="true"></span></a>
					<ul id="menu-main-menu" class="main-menu clone-main-menu ovic-clone-mobile-menu box-has-content">

						<li class="menu-item">
							<a href="<?= Url::to(['/']) ?>" class="kt-item-title ovic-menu-item-title" title="Home"><?= Yii::t('app', 'Bosh sahifa') ?></a>
						</li>
						<?php foreach ($menus as $key => $menu): ?>
							<li class="menu-item menu-item-has-children">
								<a <?php if($menu->page!=NULL){echo 'href="'.$menu->page.'"';}elseif($menu->link!=NULL){echo 'href="'.$menu->link.'"'; }else{echo 'href="#"';}?> <?php if($menu->target=='1'){echo 'target="_blank"';}?> class="kt-item-title ovic-menu-item-title">
									<?= $menu->{$lang.'_name'}?>
								</a>
								<?php $sub_menus = SubMenu::find()->where(['parent' => [$menu->id], 'active'=>'1'])->orderBy('order_m ASC')->all(); ?>
								<?php if ($sub_menus):?>
									<ul class="sub-menu">
										<?php foreach ($sub_menus as $key => $sub_menu): ?>
											<li>
												<a <?php if($sub_menu->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $sub_menu->page  ])?>"<?php endif;?> href="<?= $sub_menu->link;?>" <?php if($sub_menu->target==1):?> target="_blank"<?php endif;?>>
													<?= $sub_menu->{$lang.'_name'}; ?>
												</a>
											</li>
										<?php endforeach;?>
									</ul>
								<?php endif;?>
							</li>
						<?php endforeach;?>
						<li class="menu-item"><a href="<?=Url::to(['site/contacts'])?>"><?= Yii::t('app', 'Biz bilan bog\'lanish') ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</header>



 <div class="main-content home-page main-content-home<?= param('themeVariant')?>">
    <?= $content ?>
</div>



  <!-- Footer -->
  <footer>
		<div class="footer layout1 ">
			<div class="container">
				<div class="main-footer">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-ss-12">
							<div class="widget widget-text">
								<h3 class="widgettitle"><?= Yii::t('app', 'Biz bilan bog\'lanish') ?></h3>
								<?php foreach ($sayts as $key => $sayt):?>
								<div class="content">
									 <h5 class="subtitle"><?= Yii::t('app', 'Address'); ?></h5>  
									  <p class="des"><?= $sayt->address;?></p>  
									 <h5 class="subtitle"><?= Yii::t('app', 'Phone'); ?></h5> 
									<p class="des">
										<a href="tel:<?= $sayt->fax;?>" style="color:#aaa;"><?= $sayt->fax;?></a></p>
									 <h5 class="subtitle"><?= Yii::t('app', 'Email'); ?></h5>
									<p class="des"><?= $sayt->email_em;?></p>
								</div>
							 <?php endforeach;?>

							</div>

						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 col-ss-12">

							<div class="row auto-clear">
								
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-ts-12 ">
									<div class="widget widget-custom-menu">
										<h3 class="widgettitle"><?= Yii::t('app', 'Avtomobil turi bo\'yicha') ?></h3>
										<ul >
											<?php foreach($cars as $key => $car):?>
												<li><a href="<?= Url::to(['/site/filter', 'car_id' => $car->id])?>" ><?= $car['name_'.$lang]?></a></li>
											<?php endforeach;?>
										</ul>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-ts-12 ">
										<?php foreach ($sayts as $key => $sayt):?>
										<ul class="list-socials">
											<?php //var_dump($sayt->facebook);exit(); ?>
											<li style="color:#fff;"><a href="<?= $sayt->facebook;?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="<?= $sayt->instagram;?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
										</ul>
									<?php endforeach;?>

								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-note">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 left-content">
							<div class="coppy-right">
								<h3 class="content">© Copyright 2020 <span class="site-name"><a href="https://smarttechnology.uz" target="_blank"> Smart Technology</a></span> <span class="text"> </span><?= Yii::t('app', 'Barcha huquqlar himoyalangan') ?></h3>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 right-content">
							<ul class="list-payment">
								<li><a href="#"></a></li>
								<li></li>
								<li></li>
								<li></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-169603843-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-169603843-1');
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

