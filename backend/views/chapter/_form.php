<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Section;

/* @var $this yii\web\View */
/* @var $model backend\models\Chapter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chapter-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'section_id')->dropDownList(
        ArrayHelper::map(Section::find()->all(), 'id','uz_section'),
        ['prompt'=>'Выберите раздел']
        ) ?>

    <?= $form->field($model, 'uz_chapter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ru_chapter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_chapter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'x_chapter')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
