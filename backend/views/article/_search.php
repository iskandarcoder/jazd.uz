<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ArticleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'chapter_id') ?>

    <?= $form->field($model, 'uz_article') ?>

    <?= $form->field($model, 'ru_article') ?>

    <?= $form->field($model, 'en_article') ?>

    <?php // echo $form->field($model, 'x_article') ?>

    <?php // echo $form->field($model, 'uz_article_tex') ?>

    <?php // echo $form->field($model, 'ru_article_tex') ?>

    <?php // echo $form->field($model, 'en_article_tex') ?>

    <?php // echo $form->field($model, 'x_article_tex') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
