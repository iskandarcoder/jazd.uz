<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Chapter;
use dosamigos\ckeditor\CKEditor;


/* @var $this yii\web\View */
/* @var $model backend\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'chapter_id')->dropDownList(
        ArrayHelper::map(Chapter::find()->all(), 'id','uz_chapter'),
        ['prompt'=>'Выберите глава']
        ) ?>

    <?= $form->field($model, 'uz_article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ru_article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'x_article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uz_article_tex')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]) ?>

    <?= $form->field($model, 'ru_article_tex')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]) ?>

    <?= $form->field($model, 'en_article_tex')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]) ?>

    <?= $form->field($model, 'x_article_tex')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
