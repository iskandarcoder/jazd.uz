<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ZayavkaType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zayavka-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'zayavka_id')->textInput() ?>

    <?= $form->field($model, 'type_energy_id')->textInput() ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
