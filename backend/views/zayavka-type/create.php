<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ZayavkaType */

$this->title = 'Create Zayavka Type';
$this->params['breadcrumbs'][] = ['label' => 'Zayavka Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zayavka-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
