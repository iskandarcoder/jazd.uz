<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\User;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model backend\models\Photo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->hiddenInput(['value'=> Yii::$app->user->identity->id])->label(false);?>
    <?= $form->field($model, 'user_ids')->hiddenInput(['value'=> Yii::$app->user->identity->username])->label(false);?>

    <?php if(Yii::$app->user->identity->role_id < '3'):?>
    <div class="form-group">
    <?=$form->field($model, 'user_ids')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(User::find()->andWhere(['id'=>[35,26,27,37,2]])->all(), 'username', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите Посольство ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
        ],
            ]);
    
    ?>
    </div>
    <?php endif?>

    <?= $form->field($model, 'uz_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ru_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'x_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <div class="form-group">
    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ],
    ]); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
