<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EnergySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Energies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="energy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Energy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name_uz',
            'name_ru',
            'cost',
            'description_uz',
            //'description_ru',
            //'img1:ntext',
            //'img2:ntext',
            //'img3:ntext',
            //'img4:ntext',
            //'sell',
            //'show',
            //'active_is',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
