<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-12">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">





    <div class="form-group">
    <?= $form->field($model, 'uz_name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'ru_name')->textInput(['maxlength' => true]) ?>
    </div>


    <div class="form-group">
    <?= $form->field($model, 'uz_text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]) ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'ru_text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]) ?>
    </div>

    
    <div class="form-group">
    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ],
    ]); ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'active')->checkbox() ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div><br>

    <?php ActiveForm::end(); ?>

</div>
