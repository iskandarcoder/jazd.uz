<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Contextual classes -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= Html::encode($this->title) ?></h5>

    </div>

    <div class="panel-body">
        <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>

    <div class="table-responsive">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model){
            if($model->user_id == '37'){
                return['class'=>'danger'];
            }else {
                return['class'=>'success'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            'uz_name',
            'ru_name',
            //'en_name',
            //'x_name',
            // 'uz_text:ntext',
            // 'ru_text:ntext',
            // 'en_text:ntext',
            // 'x_text:ntext',
            // 'active',
            // 'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
