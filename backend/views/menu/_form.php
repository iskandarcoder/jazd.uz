<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Page;
use kartik\select2\Select2;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'user_id')->hiddenInput(['value'=> Yii::$app->user->identity->id])->label(false);?>
    <?= $form->field($model, 'user_ids')->hiddenInput(['value'=> Yii::$app->user->identity->username])->label(false);?>

    <?php if(Yii::$app->user->identity->role_id < '3'):?>
    <div class="form-group">
    <?=$form->field($model, 'user_ids')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(User::find()->andWhere(['id'=>[35,26,27,37,2]])->all(), 'username', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите Посольство ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
        ],
            ]);
    
    ?>
     
    </div>
    <?php endif?>

    <?= $form->field($model, 'uz_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ru_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'x_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    

    <?=$form->field($model, 'page')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Page::find()->andWhere(['user_id'=>Yii::$app->user->identity->id])->all(), 'id', 'uz_name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите Посольство ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => false,
        ],
            ]);

    ?>


    <?= $form->field($model, 'order')->textInput() ?>
    
    <?= $form->field($model, 'target')->checkbox() ?>

    <?= $form->field($model, 'sidebar')->checkbox() ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
