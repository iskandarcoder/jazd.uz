<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SaytSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sayts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sayt-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sayt', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name_ru',
            'name_uz',
            'address',
            'text_ru:ntext',
            //'text_uz:ntext',
            //'fax',
            //'email_em:email',
            //'link',
            //'facebook',
            //'twitter',
            //'instagram',
            //'logo:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
