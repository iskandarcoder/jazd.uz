<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\NewimageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Newimages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newimage-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Newimage', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'link',
            'img',
            'img2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
