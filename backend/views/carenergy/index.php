<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Energy;
use backend\models\Car;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CarenergySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Carenergies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carenergy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Carenergy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'car_id',
                'value'=>'car.name_uz',
            ],
            [
                'attribute'=>'energy_id',
                'value'=>'energy.name_uz',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
