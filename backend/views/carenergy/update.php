<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Carenergy */

$this->title = 'Update Carenergy: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Carenergies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="carenergy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
