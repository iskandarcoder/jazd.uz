<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Energy;
use backend\models\Car;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Carenergy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carenergy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'car_id')->dropDownList(
        ArrayHelper::map(Car::find()->all(), 'id','name_uz'),
        ['prompt'=>'Выберите машине']
        ) ?>

    <?= $form->field($model, 'energy_id')->dropDownList(
        ArrayHelper::map(Energy::find()->all(), 'id','name_uz'),
        ['prompt'=>'Выберите....']
        ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
