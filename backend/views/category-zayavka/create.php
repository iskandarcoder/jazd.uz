<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoryZayavka */

$this->title = 'Create Category Zayavka';
$this->params['breadcrumbs'][] = ['label' => 'Category Zayavkas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-zayavka-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
