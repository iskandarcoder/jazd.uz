<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Section */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="section-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uz_section')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ru_section')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_section')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'x_section')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
