<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\Links */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="links-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->user->identity->role_id < '3'):?>
    <div class="form-group">
    <?=$form->field($model, 'user_ids')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(User::find()->andWhere(['id'=>[35,26,27,37,2]])->all(), 'username', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите Посольство ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
        ],
            ]);

    ?>

    </div>
    <?php endif?>

    <?= $form->field($model, 'uz_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ru_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fileuz')->fileInput() ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
