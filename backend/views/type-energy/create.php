<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TypeEnergy */

$this->title = 'Create Type Energy';
$this->params['breadcrumbs'][] = ['label' => 'Type Energies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-energy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
