<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TypeEnergy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="type-energy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'amper')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
