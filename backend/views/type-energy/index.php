<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TypeEnergySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Type Energies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-energy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Type Energy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'amper',
            'weight',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
