<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\CategorNews;
use backend\models\User;
use dosamigos\ckeditor\CKEditor;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- Centered forms -->
<div class="row">
    <div class="col-md-12">
         <?php $form = ActiveForm::begin(); ?>
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">

    <div class="form-group">
    <?= $form->field($model, 'uz_thema')->textarea(['rows' => 2]) ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'ru_thema')->textarea(['rows' => 2]) ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'uz_description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'ru_description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>
    </div>


    <div class="form-group">
    <?= $form->field($model, 'uz_text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]) ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'ru_text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]) ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ],
    ]); ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'file')->fileInput() ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'slider')->checkbox() ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'active')->checkbox() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

                    </div>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
