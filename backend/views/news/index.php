<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Contextual classes -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= Html::encode($this->title) ?></h5>
    </div>

    <div class="panel-body">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

    </div>

    <div class="table-responsive">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model){
            if($model->user_id == '36'){
                return['class'=>'danger'];
            }else {
                return['class'=>'success'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            //'id',
            //'id_category_news',

            'uz_thema:ntext',
            'ru_thema:ntext',
            // 'en_thema:ntext',
            // 'x_thema:ntext',
            // 'uz_description:ntext',
            // 'ru_description:ntext',
            // 'en_description:ntext',
            // 'x_description:ntext',
            // 'uz_text:ntext',
            // 'ru_text:ntext',
            // 'en_text:ntext',
            // 'x_text:ntext',
             'date',
            // 'img',
            // 'slider',
            // 'active',
            // 'show_n',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
<!-- /contextual classes -->
