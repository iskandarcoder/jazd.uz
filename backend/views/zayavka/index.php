<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ZayavkaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявка';
$this->params['breadcrumbs'][] = $this->title;
$user = User::find()->Where(['role_id' => '5'])->orderBy('id DESC')->all();
?>
<div class="zayavka-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=Html::beginForm(['zayavka/list'],'post');?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
      <div style="float:right;">
      <select style="width:200px;" name="operator">
        <option>----</option>
        <?php foreach ($user as $key => $value):?>
        <option value="<?= $value->id;?>"><?= $value->username;?></option>
      <?php endforeach;?>
      </select>
      <div class="col-md-2" style="float: right; padding-right:20px;">
            <?=Html::submitButton('Send', ['class' => 'btn btn-info',]);?>
        </div>
    </div>
    <p>
        <?= Html::a('', [''], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model){
            if($model->status == '1'){
                return['class'=>'success'];
            }elseif($model->status == '2') {
                return['class'=>'info'];
            }elseif($model->status == '3') {
                return['class'=>'primary'];
            }else{
                return['class'=>'warning'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=>'energy_id',
                'value'=>'energy.name_uz',
            ],
            'fio',
            'telephone',
            'email:email',
            //'region_id',
            //'district_id',
            //'address',
            //'old_energy',
            //'count_energy',
            [
                'attribute'=>'status',
                'value'=>'sttatus.name',
            ],
            //'text:ntext',
            'date', 
            [
                'attribute'=>'user_id',
                'value'=>'user.username',
            ],
            ['class' => 'kartik\grid\CheckboxColumn'],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?= Html::endForm();?>
</div>
