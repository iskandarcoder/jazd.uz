<?php

namespace backend\controllers;

use Yii;
use backend\models\Energy;
use backend\models\EnergySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EnergyController implements the CRUD actions for Energy model.
 */
class EnergyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Energy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EnergySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Energy model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Energy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Energy();
        $model->active_is = 1;
        // var_dump();exit();

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $image2 = UploadedFile::getInstance($model, 'image2');
            $image3 = UploadedFile::getInstance($model, 'image3');
            $image4 = UploadedFile::getInstance($model, 'image4');
            if (!empty($image)) {
                $model->img1 = random_int(0,9999). '.' . $image->extension;
            }
             if (!empty($image2)) {
                $model->img2 = random_int(0,9999). '.' . $image2->extension;
            }
            if (!empty($image3)) {
                $model->img3 = random_int(0,9999). '.' . $image3->extension;
            }
            if (!empty($image4)) {
                $model->img4 = random_int(0,9999). '.' . $image4->extension;
            }
            $model->show = 1;
            
            if ($model->save()) {
                if (!empty($image)) {
                    $image->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img1);
                    // return $this->redirect(['index']);
                }
                if (!empty($image2)) {
                    $image2->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img2);
                    // return $this->redirect(['index']);
                }
                if (!empty($image3)) {
                    $image3->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img3);
                    // return $this->redirect(['index']);
                }
                if (!empty($image4)) {
                    $image4->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img4);
                    // return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
}

    /**
     * Updates an existing Energy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $image2 = UploadedFile::getInstance($model, 'image2');
            $image3 = UploadedFile::getInstance($model, 'image3');
            $image4 = UploadedFile::getInstance($model, 'image4');
            if (!empty($image)) {
                $model->img1 = random_int(0,9999). '.' . $image->extension;
            }
             if (!empty($image2)) {
                $model->img2 = random_int(0,9999). '.' . $image2->extension;
            }
            if (!empty($image3)) {
                $model->img3 = random_int(0,9999). '.' . $image3->extension;
            }
            if (!empty($image4)) {
                $model->img4 = random_int(0,9999). '.' . $image4->extension;
            }
            
            if ($model->save()) {
                if (!empty($image)) {
                    $image->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img1);
                    // return $this->redirect(['index']);
                }
                if (!empty($image2)) {
                    $image2->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img2);
                    // return $this->redirect(['index']);
                }
                if (!empty($image3)) {
                    $image3->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img3);
                    // return $this->redirect(['index']);
                }
                if (!empty($image4)) {
                    $image4->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img4);
                    // return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Energy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = Energy::findOne($id);
        unlink(Yii::getAlias('@frontend/web/uploads/') . $data->image);
        unlink(Yii::getAlias('@frontend/web/uploads/') . $data->image2);
        unlink(Yii::getAlias('@frontend/web/uploads/') . $data->image3);
        unlink(Yii::getAlias('@frontend/web/uploads/') . $data->image4);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Energy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Energy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Energy::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
