<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "zayavka".
 *
 * @property int $id
 * @property int $energy_id
 * @property string $fio
 * @property string $telephone
 * @property string $email
 * @property int $region_id
 * @property int $district_id
 * @property string $address
 * @property int $status
 * @property string $text
 * @property string $comment
 * @property string $date
 * @property int $user_id
 */
class Zayavka extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zayavka';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_zayavka','energy_id','category_zayavka', 'region_id', 'district_id', 'status', 'user_id'], 'required'],
            [['energy_id','category_zayavka', 'region_id', 'district_id', 'status', 'user_id'], 'integer'],
            [['text', 'comment'], 'string'],
            [['date'], 'safe'],
            [['fio', 'telephone', 'email', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'energy_id' => 'Energy ID',
            'category_zayavka' => '',
            'fio' => Yii::t('app', 'F.I.O'),
            'telephone' => Yii::t('app', 'Telefon'),
            'email' => 'Email',
            'region_id' => 'Region ID',
            'district_id' => 'District ID',
            'address' => 'Address',
            'status' => 'Status',
            'text' => 'Text',
            'comment' => 'Comment',
            'date' => 'Date',
            'user_id' => 'User ID',
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }

    public function getEnergy(){
        return $this->hasOne(Energy::className(),['id'=>'energy_id']);
    }

    public function getSttatus(){
        return $this->hasOne(Status::className(),['id'=>'status']);
    }
}
