<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Article;

/**
 * ArticleSearch represents the model behind the search form about `backend\models\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'chapter_id'], 'integer'],
            [['uz_article', 'ru_article', 'en_article', 'x_article', 'uz_article_tex', 'ru_article_tex', 'en_article_tex', 'x_article_tex'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'chapter_id' => $this->chapter_id,
        ]);

        $query->andFilterWhere(['like', 'uz_article', $this->uz_article])
            ->andFilterWhere(['like', 'ru_article', $this->ru_article])
            ->andFilterWhere(['like', 'en_article', $this->en_article])
            ->andFilterWhere(['like', 'x_article', $this->x_article])
            ->andFilterWhere(['like', 'uz_article_tex', $this->uz_article_tex])
            ->andFilterWhere(['like', 'ru_article_tex', $this->ru_article_tex])
            ->andFilterWhere(['like', 'en_article_tex', $this->en_article_tex])
            ->andFilterWhere(['like', 'x_article_tex', $this->x_article_tex]);

        return $dataProvider;
    }
}
