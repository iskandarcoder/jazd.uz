<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "car".
 *
 * @property int $id
 * @property string $name_uz
 * @property string $name_ru
 * @property string $model
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_uz', 'name_ru', 'model'], 'required'],
            [['name_uz', 'name_ru', 'model'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_uz' => 'Name Uz',
            'name_ru' => 'Name Ru',
            'model' => 'Model',
        ];
    }
}
