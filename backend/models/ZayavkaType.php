<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "zayavka_type".
 *
 * @property int $id
 * @property int $zayavka_id
 * @property int $type_energy_id
 * @property int $count
 */
class ZayavkaType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zayavka_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zayavka_id', 'type_energy_id', 'count'], 'required'],
            [['zayavka_id', 'type_energy_id', 'count'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'zayavka_id' => 'Zayavka ID',
            'type_energy_id' => 'Type Energy ID',
            'count' => 'Count',
        ];
    }

    public function getTenergy(){
        return $this->hasOne(TypeEnergy::className(),['id'=>'type_energy_id']);
    }
}
