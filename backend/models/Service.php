<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property int $user_id
 * @property string $uz_name
 * @property string $ru_name
 * @property string $en_name
 * @property string $icon_style
 * @property string $uz_description
 * @property string $ru_description
 * @property string $en_description
 * @property string $x_description
 * @property string $icon
 * @property string $link
 * @property int $target
 * @property int $page
 * @property int $order_s
 * @property string $date
 * @property int $active
 * @property string $user_ids
 *
 * @property User $user
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
     public $file;
    public static function tableName()
    {
        return 'service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uz_name', 'ru_name'], 'required'],
            [['user_id', 'target', 'page', 'order_s', 'active'], 'integer'],
            [['uz_description', 'ru_description', 'en_description', 'x_description', 'user_ids'], 'string'],
            [['date'], 'safe'],
            [['uz_name', 'ru_name','icon_style', 'icon'], 'string', 'max' => 500],
            [['link'], 'string', 'max' => 5000],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'uz_name' => 'Uz Name',
            'ru_name' => 'Ru Name',
            //'en_name' => 'En Name',
            'icon_style' => 'Icon Style',
            'uz_description' => 'Uz Description',
            'ru_description' => 'Ru Description',
            'en_description' => 'En Description',
            'x_description' => 'X Description',
            'icon' => 'Icon',
            'link' => 'Link',
            'target' => 'Target',
            'page' => 'Page',
            'order_s' => 'Order S',
            'date' => 'Date',
            'active' => 'Active',
            'user_ids' => 'User Ids',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
