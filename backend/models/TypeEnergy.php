<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "type_energy".
 *
 * @property int $id
 * @property string $amper
 * @property int $weight
 */
class TypeEnergy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type_energy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amper', 'weight'], 'required'],
            [['weight'], 'integer'],
            [['amper'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amper' => 'Amper',
            'weight' => 'Weight',
        ];
    }
}
