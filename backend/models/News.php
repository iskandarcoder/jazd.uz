<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $id_category_news
 * @property string $uz_thema
 * @property string $ru_thema
 * @property string $en_thema
 * @property string $x_thema
 * @property string $uz_description
 * @property string $ru_description
 * @property string $en_description
 * @property string $x_description
 * @property string $uz_text
 * @property string $ru_text
 * @property string $en_text
 * @property string $x_text
 * @property string $date
 * @property string $img
 * @property integer $slider
 * @property integer $active
 * @property integer $show_n
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;

    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slider', 'active', 'show_n'], 'integer'],
            //[[], 'required'],
            [['uz_thema',  'ru_thema', 'en_thema', 'x_thema', 'uz_description', 'ru_description', 'en_description', 'x_description', 'uz_text', 'ru_text', 'en_text', 'x_text'], 'string'],
            [['file'],'file'],
            [['date','user_ids'], 'safe'],
            [['img'], 'string', 'max' => 5000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_category_news' => 'Категория',
            'user_ids' => 'Посольство',
            'uz_thema' => 'Uz Thema',
            'ru_thema' => 'Ru Thema',
            'en_thema' => 'En Thema',
            'x_thema' => 'X Thema',
            'uz_description' => 'Uz Description',
            'ru_description' => 'Ru Description',
            'en_description' => 'En Description',
            'x_description' => 'X Description',
            'uz_text' => 'Uz Text',
            'ru_text' => 'Ru Text',
            'en_text' => 'En Text',
            'x_text' => 'X Text',
            'date' => 'Date',
            'img' => 'Images (width=555px; height=416px)',
            'slider' => 'Slider',
            'active' => 'Active',
            'show_n' => 'Show N',
            'file' => 'Images (width=555px; height=416px)',
        ];
    }

    public function getCategorNews(){
        return $this->hasOne(CategorNews::className(),['id'=>'id_category_news']);
    }

    public function getUser(){
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }

     public static function searchAnywhere($keyword) {
        if (!empty($keyword)) {
            $query = self::find();

            $keyword = trim($keyword);
            $keyword = htmlentities($keyword);

            $query->select('*');                  // выбираем только поле 'title'
            $query->orWhere(['like', 'uz_thema', $keyword])
            ->orWhere(['like', 'ru_thema', $keyword])
            ->orWhere(['like', 'en_thema', $keyword])
            ->orWhere(['like', 'x_thema', $keyword])
            ->orWhere(['like', 'uz_description', $keyword])
            ->orWhere(['like', 'ru_description', $keyword])
            ->orWhere(['like', 'en_description', $keyword])
            ->orWhere(['like', 'x_description', $keyword])
            ->orWhere(['like', 'uz_text', $keyword])
            ->orWhere(['like', 'ru_text', $keyword])
            ->orWhere(['like', 'en_text', $keyword])
            ->orWhere(['like', 'x_text', $keyword]);

            $modelNews = $query->asArray()->all();
        } else {
            $modelNews = FALSE;
        }
        return $modelNews;
    }
}
