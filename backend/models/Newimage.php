<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "newimage".
 *
 * @property int $id
 * @property string $link
 * @property string $img
 * @property string $img2
 */
class Newimage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $image;
    public $image2;
    public static function tableName()
    {
        return 'newimage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link', 'img', 'img2'], 'required'],
            [['link'], 'string', 'max' => 300],
            [['img', 'img2'], 'string', 'max' => 255],
            [['image','image2'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,jpeg,gif']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
            'img' => 'Img',
            'img2' => 'Img2',
        ];
    }
}
