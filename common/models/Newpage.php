<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "newpage".
 *
 * @property int $id
 * @property string $image
 * @property string $image2
 * @property string $link
 */
class Newpage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'newpage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'image2', 'link'], 'required'],
            [['image', 'image2'], 'string', 'max' => 255],
            [['link'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'image2' => 'Image2',
            'link' => 'Link',
        ];
    }
}
